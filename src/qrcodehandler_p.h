/*******************************************************************************
**
** Copyright (C) 2021-2022 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the QR Code Reader project.
**
** Redistribution and use in source and binary forms,
** with or without modification, are permitted provided
** that the following conditions are met:
**
** * Redistributions of source code must retain the above copyright notice,
**   this list of conditions and the following disclaimer.
** * Redistributions in binary form must reproduce the above copyright notice,
**   this list of conditions and the following disclaimer
**   in the documentation and/or other materials provided with the distribution.
** * Neither the name of the copyright holder nor the names of its contributors
**   may be used to endorse or promote products derived from this software
**   without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
** THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
** IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
** FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
** PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS;
** OR BUSINESS INTERRUPTION)
** HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
** WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
** EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
*******************************************************************************/

#ifndef QRCODEHANDLER_P_H
#define QRCODEHANDLER_P_H

#include <QtCore/QObject>

#include "qrcodetypes.h"

class QRCodeHandlerPrivate : public QObject
{
    Q_OBJECT

public:
    explicit QRCodeHandlerPrivate(QObject *parent = nullptr);

    void setSourceText(const QRCodeText &sourceText);

    QRCodeType type() const;
    QRCodeText sourceText() const;
    QRCodeText formatedText() const;
    QRCodeFields fields() const;
    bool executable() const;

    bool execute();

signals:
    void typeChanged(QRCodeType type);
    void sourceTextChanged(const QRCodeText &sourceText);
    void formatedTextChanged(const QRCodeText &formatedText);
    void fieldsChanged(const QRCodeFields &fields);
    void executableChanged(bool executable);

private:
    QRCodeType _determineType(const QRCodeText &sourceText);
    QRCodeText _makeFormatedText(QRCodeType type, const QRCodeFields &fields);
    QRCodeFields _parseSourceText(QRCodeType type, const QRCodeText &sourceText);
    QRCodeFields _parseText(const QRCodeText &sourceText);
    QRCodeFields _parseEMail(const QRCodeText &sourceText);
    QRCodeFields _parseLink(const QRCodeText &sourceText);
    QRCodeFields _parsePhone(const QRCodeText &sourceText);
    QRCodeFields _parseSms(const QRCodeText &sourceText);
    QRCodeFields _parseVCard(const QRCodeText &sourceText);
    QRCodeFields _parseGeo(const QRCodeText &sourceText);
    bool _executableCheck(QRCodeType type, const QRCodeFields &fields);
    bool _executeText();
    bool _executeEMail();
    bool _executeLink();
    bool _executePhone();
    bool _executeSms();
    bool _executeVCard();
    bool _executeGeo();

private:
    QRCodeType m_type { QRCodeType::Text };
    QRCodeText m_sourceText {  };
    QRCodeText m_formatedText {  };
    QRCodeFields m_fields {  };
    bool m_executable { false };
};

#endif // QRCODEHANDLER_P_H
